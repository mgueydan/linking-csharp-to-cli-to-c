This project is used to tests linking between C# -> C++/Cli -> C++.

Among other things, the linking errors are not easy to diagnose.

See : https://gitlab.com/mgueydan/linking-csharp-to-cli-to-c/-/wikis/home
